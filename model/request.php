<?php
	include_once("../model/dbconnect.php");
	include_once('../model/viewdata.php');
	define('ROOTPATH', __DIR__);
	error_reporting(0);
	class RequestHandler{
		public  $requestedData;
		public  $paginatorObj;
		public function __construct(){
			$this->requestedData = $_REQUEST;
			$this->paginatorObj = new paginator();
			$this->handleRequest();
		}
		
		public function handleRequest(){
			switch( strtoupper($this->requestedData['request-type']) ){
				case "SEARCH-RESULT"	: $this->handleSearchRequest();break;
			}
		}
		
		public function handleSearchRequest(){
				global $viewDataObj;
				
				if( isset($this->requestedData['request-data']) && ($this->requestedData['request-data']) ){
					$requestParam = $this->requestedData['request-data'];
					// to get the pagination limit
					if($requestParam['pagination']){
						$resultPerPage = $requestParam['pagination'];
					}else{
						$resultPerPage = 3;
					}
					
					if($requestParam['current_page_number']){
						$startPage = $requestParam['current_page_number'];
					}else{
						$startPage = 1;
					}
					
					// to create query on the basis of values and pagination limit
					if($requestParam['searchBy']){
						$resultRowSet	= $this->paginatorObj->getResultData($this->paginatorObj->createQuery($requestParam['searchBy'],$requestParam['txt_search'],$resultPerPage,$startPage));
						$totalPage = count($this->paginatorObj->getResultData($this->paginatorObj->createQuery($requestParam['searchBy'],$requestParam['txt_search'],null,null)));
					}else{
						$resultRowSet	= $this->paginatorObj->getResultData($this->paginatorObj->createQuery(null,null,$resultPerPage,$startPage));
						$totalPage = count($this->paginatorObj->getResultData($this->paginatorObj->createQuery(null,null,null,null)));
					}
					
					
					
				}else{
					$resultRowSet	= $this->paginatorObj->getResultData($this->paginatorObj->createQuery());
					$totalPage = count($this->paginatorObj->getResultData($this->paginatorObj->createQuery(null,null,null,null)));
					$resultPerPage = 3;
				}
				
				
				
				
				$viewDataObj->set('currentPage',$startPage);
				$viewDataObj->set('requestParam',$requestParam);
				$viewDataObj->set('resultRowSet',$resultRowSet);
				$viewDataObj->set('pagination',$resultPerPage);
				
				$viewDataObj->set(
					'paginationContent',
					$this->paginatorObj->getpaginate($startPage,ceil($totalPage/$resultPerPage))
				);
				$this->render('content.php');
		}
		
		
		public function render($page){
			global $viewDataObj;
			if(file_exists("../view/".$page)){
				include_once("../view/".$page);
			}else{
				echo "nothing found";
			}
		}
	}
	$requestHandlerObj = new RequestHandler();
?>