<?php
	class ViewData{
		public $data = array();
		public function set($pocket,$data){
			$this->data[$pocket]	= $data;
		}
		
		public function get($pocket){
			if(isset($this->data[$pocket]))
				return $this->data[$pocket];
		}
	}
	$viewDataObj = new ViewData();
?>