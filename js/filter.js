'use strict';
/* App Module */
//var myFilterss = angular.module('filters', []);

angular.module('demo')
    .filter('reverse', function(Data) {
        return function(text) {
            return text.split("").reverse().join("") + Data.message;
        };
    })
    .filter('cfilter', function() {
        return function(input, dataset) {
            return dataset;

        }
    })
    .filter('toarray', function() {
        return function(items) {
            var filtered = [];
            angular.forEach(items, function(item) {
                filtered.push(item);
            });

            return filtered;
        };
    })
    .filter('objFilter', function() {
        return function(items, filter) {
            if (!filter){
                return items;
            }
            var result = {};
            angular.forEach( filter, function(filterVal, filterKey) {
                angular.forEach(items, function(item, key) {
                    var fieldVal = item[filterKey];
                    if (fieldVal && fieldVal.toLowerCase().indexOf(filterVal.toLowerCase()) > -1){
                        result[key] = item;
                    }
                });
            });
            return result;
        };
    })
    .filter('filterDate', ["$compile", function($compile){
        return function(item, scope) {

            return (item && (item != "--") && (typeof item != "undefined")) ? item : "N/A";
        };
    }])

    .filter('icon', function($sce){
        return function(item, data, color){

            if(!color){
                color = '';
            }

            switch(item){
                case 'USD' : return $sce.trustAsHtml("<i class='fa fa-usd "+ color +"'></i> ") ;break;
                case 'ZAR' : return $sce.trustAsHtml("<i class='"+ color +"'>R</i> ");break;
                case 'XOF' : return $sce.trustAsHtml("<i class='"+ color +"'>CHF</i> ");break;
                case 'NGN' : return $sce.trustAsHtml("<strike><i class='"+ color +"'>N</i> </strike>");break;
                case 'NAD' : return $sce.trustAsHtml("<i class='fa fa-usd "+ color +"'></i> ") ;break;
                case 'MZN' : return $sce.trustAsHtml("<i class='"+ color +"'>MT</i> ");break;
                case 'EUR' : return $sce.trustAsHtml("<i class='fa fa-eur "+ color +"'></i> ") ;break;
                case 'BWP' : return $sce.trustAsHtml("<i class=' "+ color +"'><b>P</b></i> ") ;break;
                default : return item ;break;
            }
        };
    })

    .filter('getDateFormat', function(){
        return function(date){

            if(date == '' || date == 'NA') {
                return 'No Survey';
            }

            var validDateObj = new Date(date)
            if(validDateObj.getFullYear() == 1970){
                return 'No Survey';
            }

            var weekday = ['Sun','Mon','Tue','Wed','Thu','Fri', 'Sat'];
            var monthName = ['Jan','Feb','Mar','Apr','May','Jun', 'Jul','Aug','Sep','Oct','Nov','Dec'];
            date =    date.replace(/-/g, '/');
            var dateOut = new Date(date);
            var timeType = (dateOut.getHours() >= 12) ? 'PM' : 'AM';
            //var dateHour = (dateOut.getHours() >= 12) ? (dateOut.getHours() - 12) : dateOut.getHours();

            //var formatedDate = weekday[dateOut.getDay()] + ", " +
            //                  dateOut.getDate() + " " +
            //                  monthName[dateOut.getMonth()]+ " " +
            //                  dateOut.getFullYear() + " " +
            //                  dateOut.getHours() + ":" +
            //                  dateOut.getMinutes() + " " + timeType;

            var hrs = dateOut.getHours();
            if (hrs > 12) {
                hrs -= 12;
            } else if (hrs === 0) {
                hrs = 12;
            } if (hrs < 10){
                hrs = '0'+ hrs;
            }


            var mins = dateOut.getMinutes();
            if (mins < 10) {
                mins = '0'+ mins;
            }
            var dd = dateOut.getDate();
            if (dd < 10) {
                dd = '0'+ dd;
            }
            var mm = dateOut.getMonth();
            mm = mm+1;
            if (mm < 10) {
                mm = '0'+ mm;
            }

            var formatedDate =  dd + "-" +
                mm + "-" +
                dateOut.getFullYear() + "  " +
                hrs + ":" +
                mins + " " + timeType;


            return formatedDate;
        };
    })

    .filter('getTimeSection', function(){
        return function(dateValue){
            var formatedDate = dateValue.split(" ");
            return formatedDate[1];
        };
    });
