'use strict';

function IndexController($scope, $rootScope, requestHandler,$http) {
    $scope.search =  {};
    $scope.page = 1;


    $scope.searchResult = function() {

        if(typeof $scope.search.query != 'undefined'){
            //console.log(($scope.search.query).length);

            if(($scope.search.query).length < 3 || $scope.search.query == 'undefined'){
                return;
            }

        }
        $scope.loader = true;

        requestHandler.preparePostRequest({

            url: 'controller/request.php',
            data: {data:
                    {
                        requestType : 'search',
                        filter : $scope.search
                    }
            }

        }).then(function (response) {


            $scope.loader = false;

            $scope.type = response.type;
            $scope.facility = response.facility;
            $scope.city = response.city;
            $scope.rank = response.rank;
            $scope.total = response.total;
            $scope.exeTime = response.exeTime;
            $scope.result = response.result;


        }).catch(function () {

        })
    }

    $scope.getPrevious = function(){
        if($scope.page == 1){
            return;
        }

        $scope.page--;
        $scope.getPageRequest();

    }

    $scope.getNext = function(){

        if($scope.total < ($scope.page)*10){
            return;
        }

        $scope.page++;
        $scope.getPageRequest();


    }

    $scope.getPageRequest = function(){

        //console.log($scope.page);return;
        $scope.loader = true;

        requestHandler.preparePostRequest({

            url: 'controller/request.php',
            data: {data:
            {
                requestType : 'search-by-page',
                filter : $scope.search,
                page : $scope.page
            }
            }

        }).then(function (response) {
            console.log(response);

            $scope.loader = false;

            $scope.exeTime = response.exeTime;
            $scope.result = response.result;


        }).catch(function () {

        })
    }

    $scope.getTimes=function(n){
        return new Array(parseInt(n));
    };

    $scope.getNumber = function(n){//console.log(n);
        return parseInt(n);
    }
}

angular.module('demo')
    .controller('IndexController', ['$scope', '$rootScope', 'requestHandler', '$http', IndexController]);


