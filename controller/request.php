<?php
set_time_limit(0);
include_once('../config.php');
include_once("../model/db.php");
include_once('../helpers.php');
require_once '../'.LIBS.'database.php';

//dd($_REQUEST);

if($_REQUEST['data']['requestType']){
    $request = strtolower($_REQUEST['data']['requestType']);
}
switch($request){
    case 'search':
                 $obj=new requestHandler();
                 $obj->searchRecords();
    break;

    case 'search-by-page':
                $obj=new requestHandler();
                $obj->searchPageRecords();
    break;
}


class requestHandler{

    function searchRecords(){

            $condition = ' Where 1 ';

            //dd($_REQUEST['data']['filter']);

            if(isset($_REQUEST['data']['filter'])){
                foreach($_REQUEST['data']['filter'] as $key => $value){
                    switch(strtolower($key)){
                        case 'query' : $condition .= " AND ( name LIKE '%$value%' OR city_hotel LIKE '%$value%' OR address LIKE '%$value%'
                                                        OR desc_en LIKE '%$value%' OR city_unique LIKE '%$value%' OR city_preferred LIKE '%$value%' ) ";
                        continue;
                        case 'type'  : {    $count = 1;
                                            foreach($value as $key1 => $value1){

                                                if($value1 != 'false'){

                                                    if($count){
                                                        $count = 0;
                                                        $condition .= " AND ( name LIKE '%$key1%' OR desc_en LIKE '%$key1%' ";
                                                    }else{
                                                        $condition .= " OR name LIKE '%$key1%' OR desc_en LIKE '%$key1%' ";
                                                    }

                                                }

                                            }
                                            if(!$count){
                                                    $condition .= " ) ";
                                            }
                        }
                        continue;

                        case 'facility'  : {    $count = 1;
                            foreach($value as $key1 => $value1){

                                if($value1 != 'false'){

                                    if($count){
                                        $count = 0;
                                        $condition .= " AND ( name LIKE '%$key1%' OR desc_en LIKE '%$key1%' ";
                                    }else{
                                        $condition .= " OR name LIKE '%$key1%' OR desc_en LIKE '%$key1%' ";
                                    }

                                }

                            }
                            if(!$count){
                                $condition .= " ) ";
                            }
                        }
                        continue;

                        case 'city'  : {    $count = 1;
                            foreach($value as $key1 => $value1){

                                if($value1 != 'false'){

                                    if($count){
                                        $count = 0;
                                        $condition .= " AND ( city_hotel LIKE '%$key1%' ";
                                    }else{
                                        $condition .= " OR city_hotel LIKE '%$key1%' ";
                                    }

                                }

                            }
                            if(!$count){
                                $condition .= " ) ";
                            }
                        }

                        continue;

                        case 'class'  : {    $count = 1;
                            foreach($value as $key1 => $value1){

                                if($value1 != 'false'){

                                    if($count){
                                        $count = 0;
                                        $condition .= " AND ( class IN ('$key1' ";
                                    }else{
                                        $condition .= " , '$key1' ";
                                    }

                                }

                            }
                            if(!$count){
                                $condition .= " )) ";
                            }
                        }

                    }
                }
            }


            //echo $condition;exit;

            $ob = new DbOperation();

            $msc = microtime(true);

            $dataQuery = "SELECT * FROM `hotels_details` $condition LIMIT 10";
            $result = 	$ob->selectQuery($dataQuery);

            $totalResultQuery = " SELECT count(*) as total FROM `hotels_details` $condition ";
            $totalResult = 	$ob->selectQuery($totalResultQuery);

            $typeQuery = "SELECT
                              SUM(
                                IF( NAME LIKE '%hotel%' OR desc_en LIKE '%hotel%', 1, 0)) AS hotel,
                                SUM(
                                IF( NAME LIKE '%apartment%' OR desc_en LIKE '%apartment%', 1, 0)) AS apartment,
                                SUM(
                                IF( NAME LIKE '%villa%' OR desc_en LIKE '%villa%', 1, 0)) AS villa,
                                SUM(
                                IF( NAME LIKE '%Guest House%' OR desc_en LIKE '%Guest House%', 1, 0)) AS 'Guest House',
                                SUM(
                                IF( NAME LIKE '%Resort %' OR desc_en LIKE '%Resort%', 1, 0)) AS Resort
                            FROM
                              `hotels_details` $condition ";//echo $typeQuery;exit;

            $typeResult = 	$ob->selectQuery($typeQuery);

            $facilityQuery = "SELECT
                              SUM(
                                IF( NAME LIKE '%Wifi%' OR desc_en LIKE '%Wifi%', 1, 0)) AS Wifi,
                                SUM(
                                IF( NAME LIKE '%Bar%' OR desc_en LIKE '%Bar%', 1, 0)) AS Bar,
                                SUM(
                                IF( NAME LIKE '%Swimming pool%' OR desc_en LIKE '%Swimming pool%', 1, 0)) AS 'Swimming pool',
                                SUM(
                                IF( NAME LIKE '%Air Conditioner%' OR desc_en LIKE '%Air Conditioner%', 1, 0)) AS 'Air Conditioner',
                                SUM(
                                IF( NAME LIKE '%Free Parking %' OR desc_en LIKE '%Free Parking%', 1, 0)) AS 'Free Parking'
                            FROM
                              `hotels_details` $condition ";//echo $facilityQuery;exit;

            $facilityResult = 	$ob->selectQuery($facilityQuery);

            $cityQuery = " SELECT
                              hotels_details.`city_hotel`,
                              COUNT(*) AS total
                            FROM
                              `hotels_details` $condition
                            GROUP BY city_hotel
                            ORDER BY total DESC
                            LIMIT 5 ";

            $cityResult = 	$ob->selectQuery($cityQuery);

            $rankQuery = " SELECT
                              hotels_details.`class`,
                              COUNT(*) AS total
                            FROM
                              `hotels_details` $condition
                            GROUP BY class
                            ORDER BY total DESC
                            LIMIT 5 ";

            $rankResult = 	$ob->selectQuery($rankQuery);

            $msc = microtime(true)-$msc;
            //echo $msc . ' s'; // in seconds
            $msc = ($msc * 1000); // in millseconds

            $response= new stdClass();
            $response->type= $typeResult;
            $response->facility= $facilityResult;
            $response->city= $cityResult;
            $response->rank= $rankResult;
            $response->result = $result;
            $response->total = $totalResult[0]['total'];
            $response->exeTime = round($msc);

        die(json_encode($response));
    }

    function searchPageRecords(){
        $condition = ' Where 1 ';

        //dd($_REQUEST['data']['filter']);

        if(isset($_REQUEST['data']['filter'])){
            foreach($_REQUEST['data']['filter'] as $key => $value){
                switch(strtolower($key)){
                    case 'query' : $condition .= " AND ( name LIKE '%$value%' OR city_hotel LIKE '%$value%' OR address LIKE '%$value%'
                                                        OR desc_en LIKE '%$value%' OR city_unique LIKE '%$value%' OR city_preferred LIKE '%$value%' ) ";
                        continue;
                    case 'type'  : {    $count = 1;
                        foreach($value as $key1 => $value1){

                            if($value1 != 'false'){

                                if($count){
                                    $count = 0;
                                    $condition .= " AND ( name LIKE '%$key1%' OR desc_en LIKE '%$key1%' ";
                                }else{
                                    $condition .= " OR name LIKE '%$key1%' OR desc_en LIKE '%$key1%' ";
                                }

                            }

                        }
                        if(!$count){
                            $condition .= " ) ";
                        }
                    }
                        continue;

                    case 'facility'  : {    $count = 1;
                        foreach($value as $key1 => $value1){

                            if($value1 != 'false'){

                                if($count){
                                    $count = 0;
                                    $condition .= " AND ( name LIKE '%$key1%' OR desc_en LIKE '%$key1%' ";
                                }else{
                                    $condition .= " OR name LIKE '%$key1%' OR desc_en LIKE '%$key1%' ";
                                }

                            }

                        }
                        if(!$count){
                            $condition .= " ) ";
                        }
                    }
                        continue;

                    case 'city'  : {    $count = 1;
                        foreach($value as $key1 => $value1){

                            if($value1 != 'false'){

                                if($count){
                                    $count = 0;
                                    $condition .= " AND ( city_hotel LIKE '%$key1%' ";
                                }else{
                                    $condition .= " OR city_hotel LIKE '%$key1%' ";
                                }

                            }

                        }
                        if(!$count){
                            $condition .= " ) ";
                        }
                    }

                        continue;

                    case 'class'  : {    $count = 1;
                        foreach($value as $key1 => $value1){

                            if($value1 != 'false'){

                                if($count){
                                    $count = 0;
                                    $condition .= " AND ( class IN ('$key1' ";
                                }else{
                                    $condition .= " , '$key1' ";
                                }

                            }

                        }
                        if(!$count){
                            $condition .= " )) ";
                        }
                    }

                }
            }
        }


        //echo $condition;exit;

        $limit = '0';
        if(isset($_REQUEST['data']['page'])){

            $limit = ($_REQUEST['data']['page'] -1 )*10;

        }



        $ob = new DbOperation();

        $msc = microtime(true);

        $dataQuery = "SELECT * FROM `hotels_details` $condition LIMIT  $limit , 10";//echo $dataQuery;exit;
        $result = 	$ob->selectQuery($dataQuery);

        $msc = microtime(true)-$msc;
        //echo $msc . ' s'; // in seconds
        $msc = ($msc * 1000); // in millseconds

        $response= new stdClass();
        $response->result = $result;
        $response->exeTime = round($msc);

        die(json_encode($response));
    }

}

?>