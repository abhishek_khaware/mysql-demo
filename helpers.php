<?php
@session_start();
if (!function_exists('debug_r')) {
    function debug_r($data, $exitFlag = true)
    {
        echo "<pre>";
        print_r($data);
        echo "</pre>";

        if ($exitFlag)
            exit;
    }
}

if (!function_exists('dd')) {

    /**
     * This function is used for debugging. It prints the data and exit the application.
     * <br>Created By- Abhishek Khaware
     * @param mixed $data
     */
    function dd($data)
    {
        echo "<pre>";
        print_r($data);
        exit;
    }
}
if (!function_exists('pp')) {

    /**
     * This function is used for debugging. It prints the data and exit the application.
     * <br>Created By- Abhishek Khaware
     * @param mixed $data
     */
    function pp($data)
    {
        echo "<hr><pre>";
        var_dump($data);
        echo "</pre>";
    }
}

if (!function_exists('decimal_format')) {
    /**
     * This function is used to get decimal with precision value without round up fraction values.
     * <br>Created By- Abhishek Khaware
     * @param $number
     * @param int $precision
     * @param string $prefix
     * @return string
     */
    function decimal_format($number, $precision=2, $prefix='')
    {
        $number=explode('.',$number);
        $toReturn= $prefix.''.$number[0];
        if(isset($number[1]) && $precision >0){
            $toReturn .= '.'.substr($number[1],0,$precision);
        }
        return $toReturn;

    }
}

if (!function_exists('implode_key')) {
    /**
     * It will implode array key with $glue and prefix $prefix
     * <br>Created By- Abhishek Khaware
     * @param $data
     * @param string $glue
     * @param string $prefix
     * @return string
     */
    function implode_key($data,$glue=', ',$prefix='')
    {
        return implode($glue, array_map(function ($v, $k)use($prefix) { return sprintf($prefix."%s='%s'", $k, $v); }, $data, array_keys($data)));
    }
}
if (!function_exists('uc')) {
    /**
     *
     * It will capitalize the string, eg. Atm nAME to ATM NAME
     * <br>Created By- Abhishek Khaware
     *
     * @param string $string the string that needs to capitalize.
     * @return string
     */
    function uc($string)
    {
        return strtoupper(strtolower($string));
    }
}

if (!function_exists('strpos_array')) {
    /**
     * This function is capable of finding an array of $needle in the array of $haystack and returns boolean.
     * <br>Created By- Abhishek Khaware
     * @param $haystack
     * @param $needle
     * @param int $offset
     * @return bool
     */
    function strpos_array($haystack, $needle, $offset=0) {
        if(!is_array($needle)) $needle = array($needle);
        foreach($needle as $query) {
            if(strpos($haystack, $query, $offset) !== false) return true; // stop on first true result
        }
        return false;
    }
}
if (!function_exists('array_only')) {
    function array_only($array, $keys)
    {
        return array_intersect_key($array, array_flip((array) $keys));
    }
}

/**
 * This file is part of the array_column library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) 2013 Ben Ramsey <http://benramsey.com>
 * @license http://opensource.org/licenses/MIT MIT
 */

if (!function_exists('array_column')) {

    /**
     * Returns the values from a single column of the input array, identified by
     * the $columnKey.
     *
     * Optionally, you may provide an $indexKey to index the values in the returned
     * array by the values from the $indexKey column in the input array.
     *
     * @param array $input A multi-dimensional array (record set) from which to pull
     *                     a column of values.
     * @param mixed $columnKey The column of values to return. This value may be the
     *                         integer key of the column you wish to retrieve, or it
     *                         may be the string key name for an associative array.
     * @param mixed $indexKey (Optional.) The column to use as the index/keys for
     *                        the returned array. This value may be the integer key
     *                        of the column, or it may be the string key name.
     * @return array
     */
    function array_column($input = null, $columnKey = null, $indexKey = null)
    {
        // Using func_get_args() in order to check for proper number of
        // parameters and trigger errors exactly as the built-in array_column()
        // does in PHP 5.5.
        $argc = func_num_args();
        $params = func_get_args();

        if ($argc < 2) {
            trigger_error("array_column() expects at least 2 parameters, {$argc} given", E_USER_WARNING);
            return null;
        }

        if (!is_array($params[0])) {
            trigger_error('array_column() expects parameter 1 to be array, ' . gettype($params[0]) . ' given', E_USER_WARNING);
            return null;
        }

        if (!is_int($params[1])
            && !is_float($params[1])
            && !is_string($params[1])
            && $params[1] !== null
            && !(is_object($params[1]) && method_exists($params[1], '__toString'))
        ) {
            trigger_error('array_column(): The column key should be either a string or an integer', E_USER_WARNING);
            return false;
        }

        if (isset($params[2])
            && !is_int($params[2])
            && !is_float($params[2])
            && !is_string($params[2])
            && !(is_object($params[2]) && method_exists($params[2], '__toString'))
        ) {
            trigger_error('array_column(): The index key should be either a string or an integer', E_USER_WARNING);
            return false;
        }

        $paramsInput = $params[0];
        $paramsColumnKey = ($params[1] !== null) ? (string) $params[1] : null;

        $paramsIndexKey = null;
        if (isset($params[2])) {
            if (is_float($params[2]) || is_int($params[2])) {
                $paramsIndexKey = (int) $params[2];
            } else {
                $paramsIndexKey = (string) $params[2];
            }
        }

        $resultArray = array();

        foreach ($paramsInput as $row) {

            $key = $value = null;
            $keySet = $valueSet = false;

            if ($paramsIndexKey !== null && array_key_exists($paramsIndexKey, $row)) {
                $keySet = true;
                $key = (string) $row[$paramsIndexKey];
            }

            if ($paramsColumnKey === null) {
                $valueSet = true;
                $value = $row;
            } elseif (is_array($row) && array_key_exists($paramsColumnKey, $row)) {
                $valueSet = true;
                $value = $row[$paramsColumnKey];
            }

            if ($valueSet) {
                if ($keySet) {
                    $resultArray[$key] = $value;
                } else {
                    $resultArray[] = $value;
                }
            }

        }

        return $resultArray;
    }

}

function storeDataUrl(array $fileList)
{
    $dataToReturn = array();

    foreach($fileList as $key => $dataFile){
        $dataFile['fileName']= date('Y-m-d H:i:s').'_'.str_replace(' ','_',$dataFile['fileName']);
        list($dataFile['fileType'], $dataFile['dataUrl']) = explode(';', $dataFile['dataUrl']);
        list(, $dataFile['dataUrl'])      = explode(',', $dataFile['dataUrl']);
        $dataFile['dataUrl'] = base64_decode($dataFile['dataUrl']);
//        array_push($dataToReturn,UPLOAD_PATH.md5($dataFile['fileName']));
        $dataToReturn[$key] = UPLOAD_PATH.md5($dataFile['fileName']);
        file_put_contents(str_replace(array('/pos','/atm','\pos','\atm'),'',UPLOAD_DIR).md5($dataFile['fileName']), $dataFile['dataUrl']);
    }

    return $dataToReturn;
}

function storeSurveyImages(array $fileList)
{
    $response = new stdClass();
    if($fileList){
        $allowed = array(IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG);
        foreach($_FILES as $key => $file){
            $fileType = exif_imagetype($file["tmp_name"]);

//            if (!in_array($fileType, $allowed) || $file["error"] !== UPLOAD_ERR_OK) {
//                $response->error=true;
//                $response->message="Wrong image file. Try again later!!";
//                break;
//            }
            $name = date('Y-d-m H:i:s').'_'.preg_replace("/[^A-Z0-9._-]/i", "_", $file["name"]);

            if(move_uploaded_file($file["tmp_name"],str_replace(array('/pos','/atm','\pos','\atm'),'',UPLOAD_DIR) . md5($name))){
                $response->error=false;
                $response->message="Uploaded Successfully.";
                $response->imgPath[str_replace(array('photoProof_','image_'),'',$key)]=UPLOAD_PATH . md5($name);
            }
            // set proper permissions on the new file
//            chmod(UPLOAD_DIR . md5($name), 0644);
        }
    }
    return $response;
}

function sendMail($param=null){
    $param->sendTo = 'hskp770@gmail.com,abhishek@successivesoftwares.com';
//    $header  = "MIME-Version: 1.0\r\n";
//    $header .= "Content-type: text/html; charset: utf8\r\n";
//
//    if(mail($param->sendTo,$param->subject,$param->content,$header)){
//        return true;
//    }else{
//        return false;
//    }


    try{
        require_once 'Mailgun/autoload.php';
//        $client = new \Http\Adapter\Guzzle6\Client();

        $mgClient = new Mailgun\Mailgun(MAILGUN_API_KEY);
        # Next, instantiate a Message Builder object from the SDK.

        $messageBldr = $mgClient->MessageBuilder();
        # Define the from address.
        $messageBldr->setFromAddress(MAILGUN_FROM);
//        $param->sendTo = 'abhishek@successivesoftwares.com';
        $param->ccEmail=null;
        $param->bccEmail=null;
        # Define a to recipient.
        $messageBldr->addToRecipient($param->sendTo);

        # Define a cc recipient.
        if($param->ccEmail && $param->ccEmail != 'NULL')
        {
            $messageBldr->addCcRecipient($param->ccEmail);
        }
        # Define a bcc recipient.
        if($param->bccEmail && $param->bccEmail != 'NULL'){
            $messageBldr->addBccRecipient($param->bccEmail);
        }
        # Define the subject.
        $messageBldr->setSubject($param->subject);
        # Define the body of the message.
        $messageBldr->setHtmlBody($param->content);

        # Finally, send the message.
        $mgClient->post(MAILGUN_DOMAIN_NAME."/messages", $messageBldr->getMessage(), $messageBldr->getFiles());

        return true;
    } catch (Exception $e){
//        dd($e->getMessage());
//        throw new Exception('Sending Failed!! '.$e->getMessage());
//        return false;
    }
    return false;
}

function setSession($key,$value){
    $_SESSION['printing'][$key] = $value;

}
function destroySession(){
    $_SESSION['printing']= null;

}
function getSession($key){

    if(isset($_SESSION['printing']) && isset($_SESSION['printing'][$key])){
        return $_SESSION['printing'][$key];
    }
    return false;
}
function setCookies($key,$value){
    $date_of_expiry = time() + 60 * 60 * 24 * 30 ;
    setcookie( $key, $value, $date_of_expiry );
}
function destroyCookies($key){
    $date_of_expiry = time() - 60 ;
    setcookie( $key, null, $date_of_expiry);
}
function getCookies($key){
    if($_COOKIE[$key]){
        return $_COOKIE[$key];
    }
    return null;
}

function isUserCached(){
    if(getCookies('isLogin')){
        setSession('isLogin',true);
        setSession('user',unserialize(getCookies('user')));
        return true;
    }
    return false;
}