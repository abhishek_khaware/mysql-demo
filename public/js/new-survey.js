
myAppVar = angular.module("printApp", ['angular.filter']);
//CashierAppVar = angular.module("printApp", []);

//Angular App Module and Controller

myAppVar.controller('PrintCtrl', function ($scope, $rootScope, $location, $http) {
  $scope.counter = 0;
  $scope.questionResult = {};
  $scope.resultToShow = {};
  $scope.search ='';
  $scope.print_for ='';

  $scope.result = false;
  $rootScope.baseUrl = $location.$$absUrl;
  //$("#door").css("display", "block");

  $scope.submitResult = function($printFor){
  $scope.resultToShow['print_for']  = $printFor;
      var formData = new FormData();
      formData.append('requestType','processPrinting');
      formData.append('sticker-design-img',JSON.stringify($scope.objectKeys($scope.questionResult['sticker-design'])));
      formData.append('sticker-scheme-img',JSON.stringify($scope.objectKeys($scope.questionResult['sticker-schemes'])));
      formData.append('sticker-design-path',JSON.stringify($scope.objectKeys($scope.questionResult['sticker-design-path'])));
      $.each($scope.resultToShow ,function(key,value){
           //console.log(key,value);
        if(typeof value == "object"){
          formData.append(key, JSON.stringify(value));
        }else{
          formData.append(key, value);
        }
      });
    $http({
      method  : 'POST',
      url     : "controller/request.php",
      data    : formData,
      processData: false,
      contentType: false,
      headers: {'Content-Type': undefined},
      transformRequest: angular.identity
    }).then(function(response){
      //console.log(response);
      if (response.status == 200 && response.data.error == false){
        sweetAlert("Thank you.", "For submitting your requirements.", "success");
      }else {
        sweetAlert("Opps..", "Something went wrong, Please try again later!", "error");
      }
      setTimeout(function () {
        window.location.reload(true);
      },5000);
    }).catch(function() {
      sweetAlert("Opps..", "Something went wrong, Please try again later!", "error");
    });

  };
  $scope.objectKeys = function(obj){
    return Object.keys(obj);
  };
  $scope.showResult = function($container){
    //console.log($scope.questionResult);
    //$('#doorList,#cashierList').hide();
      $scope.resultToShow = {};
    $scope.resultToShow['print_for'] = $container;
    angular.forEach($scope.questionResult, function(value, key) {

      //console.log(value, key);
      var qValue = value ;
      //console.log(typeof value);

      if(typeof value === 'object' && key !== 'sticker-design-quantity' && key !== 'sticker-design-path') {
        var qValue = '';
        angular.forEach(value, function (value1, key1) {

          if (value1) {

            if (qValue)
              qValue += ' + ';

            qValue += key1.replace(/_logo|.png|.jpg|_svg+/gi, '')
          }

        });
      }

      $scope.resultToShow[key.replace(/[^a-zA-Z 0-9]+/g,'_')] = qValue;
    });

    //console.log($scope.resultToShow);

    $scope.result =true;
  };

  $scope.goBack = function($container){
    $('#'+$container).show();
    $scope.result =false;
  };
  $scope.getDesignImagePath = function($img){

    return 'public/img/'+$scope.print_for+'/'+$scope.questionResult['sticker-design-path'][$img]['dir']+'/'+$img;
  };
  $scope.checkType = function($var,$type){
    return typeof $var === $type;
  };
  $scope.applyFilter = function($container,$metric){
    if($metric == 'sticker-design'){
      //console.log($container);
      $scope.questionResult['sticker-design-quantity'] = $scope.questionResult['sticker-design-quantity'] || {};
      $scope.questionResult['sticker-design-path'] = $scope.questionResult['sticker-design-path'] || {};
      if($scope.questionResult['sticker-design'][$container.img]== true){
        $scope.questionResult['sticker-design-quantity'][$container.img]= $container.quantity || 1;
        $scope.questionResult['sticker-design-path'][$container.img] = $scope.questionResult['sticker-design-path'][$container.img] || {};
        $scope.questionResult['sticker-design-path'][$container.img]['for']= $scope.questionResult['sticker-design-path'][$container.img]['for'] || {};
        $scope.questionResult['sticker-design-path'][$container.img]['for']= $scope.print_for;
        $scope.questionResult['sticker-design-path'][$container.img]['dir']= $scope.questionResult['sticker-design-path'][$container.img]['dir'] || {};
        $scope.questionResult['sticker-design-path'][$container.img]['dir']= $container.dir;
        $container.quantity = $scope.questionResult['sticker-design-quantity'][$container.img];

      }else{
        $scope.questionResult['sticker-design-quantity'][$container.img]= $container.quantity = 0;
        delete $scope.questionResult['sticker-design-quantity'][$container.img];
        delete  $scope.questionResult['sticker-design-path'][$container.img];
      }
      //console.log($scope.questionResult);

    }
    if($metric == 'sticker-schemes'){
      //console.log($scope.questionResult['sticker-schemes']);

      $scope.search = $.map($scope.questionResult['sticker-schemes'],function(value, key){
        return (value)? key.replace(/_logo|.png|.jpg|_svg+/gi, '') : null ;
      }).join('_');

      if ($scope.search == ''){
        $('.question-text-22,.question-text-4,.question-text-23,.question-text-5').hide();
      }

    }
    //console.log($container,$metric);
  };
  $scope.getVal=function($container){
      $scope.print_for = $container;
    if($container == 'door' ){
      $http.get("my-question.json").success(function(response){
        $scope.questionResult = {};
        $scope.listQuestion = response;  //ajax request to fetch data into $scope.data
        $scope.resultToShow =[];
        $scope.search ='';
        $scope.result =false;
        $scope.questionResult['sticker-schemes'] = {"mada.png": false, "here.jpg": false, "visa.png": false, "mc.png": false, "amex.png": false,"upi.png": false };

      });

    }else if($container == 'cashier' ){
      $http.get("cashier-question.json").success(function(response){
        $scope.questionResult = {};
        $scope.listQuestion = response;  //ajax request to fetch data into $scope.data
        $scope.result =false;
        $scope.resultToShow =[];
        $scope.search ='';
        $scope.questionResult['sticker-schemes'] = {
          "mada.png": false,
          "atheer.png": false,
          "naqd.png": false,
          "visa.png": false,
          "mc.png": false ,
          "amex.png": false,
          "upi.png": false
        };
      });
    }

  };

  //console.log($scope.listQuestion);

}).filter('nochar', function () {
  return function (value) {
    return (!value) ? '' : value.replace(/_/g, ' ');
  };
})
.filter('typeOf', function() {
  return function(input) {
    return  typeof input;
  };
});


